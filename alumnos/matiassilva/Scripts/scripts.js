var items = [];
var btnAgregarItem = document.getElementById('btnAgregar');
var listaItems=document.getElementById('listaItems');
var txtoItem = document.getElementById("txtItem");
var indexEdit;

const agregarItem = () => {
  if(btnAgregarItem.innerText=='Agregar'){
      var esteItem = txtoItem.value;
      
      if (esteItem !== '') {
        items.push(esteItem);
        $('#modalCambios').modal('show');
    }
  }
  else{
    items.splice(indexEdit,1,txtoItem.value);
    btnAgregarItem.innerText='Agregar';
    $('#modalCambios').modal('show');
  }
  dibujarLista();
  txtoItem.value='';
}
btnAgregarItem.addEventListener('click',()=>agregarItem());

const dibujarLista=()=>{
            
    listaItems.innerHTML='';
    items.forEach((item, index)=>{
    //agregando row  
    var div= document.createElement('div');
    div.className="row justify-content-md-center";
    //agregando columna con descripción de item
    var divp=document.createElement('div');
    divp.className="col-3";
    var pItem=document.createElement('p');
    pItem.innerText=item;
    divp.append(pItem);
    div.append(divp);
    //Agregando col con botón editar
    var divEditar=document.createElement('div');
    divEditar.className="col-1";
    var btnEditar=document.createElement('button');
    btnEditar.className="btn btn-info";
    btnEditar.innerText='Editar';
    btnEditar.addEventListener('click', ()=>editarItem(index));
    divEditar.append(btnEditar);
    div.append(divEditar);
    //Agregando col con botón eliminar
    var divEliminar=document.createElement('div');
    divEliminar.className="col-1";
    var btnEliminar=document.createElement('button');
    btnEliminar.className="btn btn-danger";
    btnEliminar.innerText='Eliminar';
    btnEliminar.addEventListener('click',()=>eliminarItem(index));
    divEliminar.append(btnEliminar);
    div.append(divEliminar);
    //agregando nuevo item al listado
    listaItems.append(div);
    });
}

function eliminarItem(index){
            
    items.splice(index,1);
    dibujarLista();

}
function editarItem(index){
    btnAgregarItem.innerText='Guardar';
    txtoItem.value = items[index]; 
    indexEdit=index;
}

