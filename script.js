const listado = document.getElementById('listado');

let alumnosTPs = [
  {
    nombre: 'matiassilva',
    linkDemo: 'alumnos/matiassilva/index.html'
  },
];

const dibujarListado = () => {
  alumnosTPs.sort((a, b) => a.nombre.localeCompare(b.nombre)).forEach((alumno) => {
    agregarItem(alumno);
  })
}

const agregarItem = (alumno) => {
  const col4container = document.createElement('div');
  col4container.classList.add('col-4', 'd-flex', 'justify-content-center');

  const cardContainer = document.createElement('div');
  cardContainer.classList.add('card');
  cardContainer.style.width = '18rem';

  const cardBodyNombre = document.createElement('div');
  cardBodyNombre.classList.add('card-body');

  const cardBodyNombreH5 = document.createElement('h5');
  cardBodyNombreH5.classList.add('card-title', 'text-center');
  cardBodyNombreH5.innerText = alumno.nombre;

  const cardBodyLink = document.createElement('div');
  cardBodyLink.classList.add('card-body', 'd-flex', 'justify-content-center');

  const cardBodyLinkA = document.createElement('a');
  cardBodyLinkA.classList.add('btn', 'btn-primary');
  cardBodyLinkA.innerText = 'DEMO';
  cardBodyLinkA.href = alumno.linkDemo;

  cardBodyNombre.append(cardBodyNombreH5);
  cardBodyLink.append(cardBodyLinkA);

  cardContainer.append(cardBodyNombre);
  cardContainer.append(cardBodyLink);

  col4container.append(cardContainer);

  listado.append(col4container);
}

dibujarListado();